FROM ubuntu:20.04

ENV ANSIBLE_FORCE_COLOR="true"
ENV ANSIBLE_HOST_KEY_CHECKING="false"
ENV ANSIBLE_CONFIG="/ansible.cfg"

COPY ansible.cfg /

RUN apt -y update && \
    apt install -y curl git python3 pip && \
    python3 -m pip install ansible && \
    apt autoclean && apt autoremove -y && \

    ansible-galaxy collection install community.general && \
    ansible-galaxy collection install community.mysql


CMD ["/bin/bash"]